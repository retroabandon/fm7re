FM-8/7/77 Hardware Projects
===========================

- `trace-board.pdf`: Instruction step/trace board using IRQs.
  _I/O_ 1985-11.
- `fm7-8088.pdf`: FM-7/NEW7/77 8088 CPU card articles from _I/O._
  - 1985-11: 8080 hardware.
  - 1985-12: CP/M-86.
  - 1986-01: CP/M-80 on NEC v30/V20
  - 1986-03: CP/M-80 emulator on CP/M-86 + NEC V30/V20.
  - 1986-09: MS-DOS on FM-7.
