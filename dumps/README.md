FM-7 ROM Dumps
==============

This directory contains raw ROM dumps from the FM-7.

The following dumps were made by cuba200611 from ROM chips pulled from an
FM-7 and read in a ROM programmer. (They were read three times each, and
checked to confirm that the CRCs of all reads matched.) Thus they are the
true contents, and will be slightly different from what you see when
inspecting memory in a running FM-7, where the address decoding circuitry
and boot switch settings partially determine what you see.

- `M151.BIN` (32K): Main CPU BASIC and BIOS ROM.
- `M154.BIN` (8K): Sub-CPU program ROM ("CRTモニタ"). Mapped into
  $E000-$FFFF in the sub-CPU address space. (SS:1-6). According to the
  schematics it can be either a MBM2764 (DIP-28 JEDEC EPROM pinout) or 2364
  (DIP-24 JEDEC PROM pinout); there are jumpers on the board to allow
  either to work in the DIP-28 socket. (This dump is from a 2364.)
- `TK11_11_M139.BIN` (0.5K). Main CPU address-deocding ROM. See
  [DECODE](../DECODE.md) for information about the contents. Each of the
  512 4-bit words is stored in the low 4 bits of each byte in the dump.
- `TL11_11_M152.BIN` (2K): Main CPU boot ROM. This is organized into four
  banks of 512 bytes, selected by switches 1 and 2 of DIP switch `SW2` on
  the back of the unit. (See the [top-level README](../README.md) for more
  details.)
- `TM11_12_M153.BIN` (4K): Sub-CPU character ROM. The lower 2K is mapped
  into $D800-$DFFF in the sub-CPU's address space. The upper 2K is
  inaccessible by the FM-7 (address line `A11` is tied to ground) but
  contains data.
