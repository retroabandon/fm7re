Fujitsu FM-7/FM77 Reverse Engineering
=====================================

This repository contains disassemblies of various bits of code from
the [Fujitsu FM-7][wpen] and FM77 series of computers. (These are
documented in more detail on the [Japanese Wikipedia page][wpja].)
These 1980s 8-bit microcomputers used a 6809 microprocessor.

References of the form _SS:n-m_ are to the [富士通 FM-7
ユーザーズマニュアル システム仕様][fm7sysspec] (Fujitsu FM-7 Users
Manual and System Specification), Fujitsu, 1982.

There's not much English-language material on the FM-7 series on the
web, but you can find some on [lgreen's FM-7 page][lgreen] and under
[`8bit/fm7/`][sedoc-fm7] in cjs's `sedoc` repo.

Other References:
- [FM-7/8 Subシステムメンテナンスコマンド][kasai]
  Sending commands to the sub-CPU; undocumented "YAMAUCHI" command;
  sample program to dump sub-CPU BIOS ROM.


Files
-----

The disassemblies, `*.dis` files, are committed to the repo for easy
viewing on the web. These are generated from `*.bin` (binary code) and
`*.info` (annotation) files, with `common.info` holding common
information used in multiple disassemblies.

`schematics/` contains schematics from various sources; see the
[REAMDE][sm/r] in that subdirectory for more details. `datasheet/`
contains datasheets for various parts used in the FM series.

The split boot ROM dumps under `boot-rom/` are extracted from
`dumps/TL11_11_M152.BIN`. These contain the hardware-generated reset
address at $FFFE/$FFFF, rather than the actual ROM contents. From the
`xm7win32.zip` archive on [lgreen's FM-7 page][lgreen], the actual ROM
contents appear to be $FFFF, but that's ignored. (The lgreen dumps are
probably from the actual ROM itself, possibly from an FM77AV40, perhaps
taken from the jcec.co.uk images, still [available from archive.org][jcec])
or perhaps he dumped them from his own original FM-7.)

These boot ROM dumps are prefixed by the switch settings that select
each one; see the table below for the filenames.

For more dumps, see the [`dumps/` subdirectory](./dumps/) and its
[REAMDE](./dumps/README.md).


High Memory Organization
------------------------

The following is specifically related to ROM decoding; see the [memory map
(SS:1-6)][ss:1-6] and following text descriptons for more general information.

- $8000-$FBFF is mapped to either system RAM (write $FD0F) or the 32K
  BIOS/BASIC ROM `M151` (read $FD0F). The inital state is determined by the
  boot swtiches; see below. ([SS:1-25])
- The $FC00-$FFFF range is decoded by address-decoding ROM `M139`. See
  [`DECODE`](DECODE.md) for full details.
  - $FC00-$FC7F: System RAM
  - $FC80-$FCFF: Shared RAM for main/sub-CPU communication
  - $FD00-$FDFF: I/O area for internal and expansion card devices
  - $FE00-$FFDF: Boot ROM. Mapped to first 480 bytes of one of the ROM's
    four 512 byte banks, selected by switches on the back of the machine.
  - $FFE0-$FFEF: System RAM
  - $FFF0-$FFFD: System RAM (interrupt vectors)
  - $FFFE-$FFFF: Boot ROM (reset vector, $FE00 in all four banks).

### Boot ROM Banks and Contents

The banks are selected by switches 1 and 2 on `SW2` on the back of the
machine (●=off/open, ○=on/closed):

        1  2  Bank    Boot ROM  MainROM  boot-rom/      Contents
        ○  ○   0    $000 - $1FF   ENA    11-fbasic.bin  Disk/ROM BASIC boot
        ●  ○   1    $200 - $3FF    -     01-bubble.bin  Bubble memory boot
        ○  ●   2    $400 - $5FF    -     10-dos.bin     DOS boot
        ●  ●   3    $600 - $7FF    -     00-halt.bin    Halt

These switches also run through NOR gate `M120` to the data pin of
flip-flop `M132` to give it its reset setting, so if both switches are
on/closed (setting D/Q=high) main ROM is enabled, otherwise $8000-$FC7F
will be main RAM. ([SS:1-25])

All banks contain the following. Addresses are the address within the ROM
bank; system address given in parens.

- $1E0-$1FD ($FFE0-$FFFD): $FF bytes. (Unused because this area is always
  decoded as system RAM--but confirm with `M139` dump).
- $1FE-$1FF ($FFFE-$FFFF): $FE00. (6809 reset vector, but unused due to
  hardcoded reset circuitry.)

The individual bank contents are:

- __Bank 0__: Disk/ROM BASIC boot. If floppy drive present with diskette,
  boots that, otherwise enters ROM BASIC.
- __Bank 1__: Bubble memory boot;
  generates blank screen and sustained tone if that fails.
- __Bank 2__: DOS boot. Attempts to read and boot from floppy drive;
  generates blank screen and sustained tone if that fails.
- __Bank 3__: Halt. Starts with $20 $FE (`BRA` to itself) and then contains
  $FF bytes up to $1E0.



<!-------------------------------------------------------------------->
[SS:1-25]: https://archive.org/stream/FM7SystemSpecifications#page/n38/mode/1up
[SS:1-6]: https://archive.org/stream/FM7SystemSpecifications#page/n19/mode/1up
[SS:1-7]: https://archive.org/stream/FM7SystemSpecifications#page/n18/mode/1up
[fm7sysspec]: https://archive.org/stream/FM7SystemSpecifications
[fm7w-hw]: https://web.archive.org/web/20160719040422/http://www23.tok2.com/home/fm7emu/ysm7/ysm72/ysm72.htm#TOP
[jcec]: https://web.archive.org/web/20171228033858/www.jcec.co.uk/fm7emu.html
[kasai]: https://kasayan86.web.fc2.com/old/fmsubsystem.htm
[lgreen]: http://www.nausicaa.net/~lgreenf/fm7page.htm
[sedoc-fm7]: https://github.com/0cjs/sedoc/blob/master/8bit/fm7/
[sm/r]: ./schematics/README.md
[wpen]: https://en.wikipedia.org/wiki/FM-7
[wpja]: https://ja.wikipedia.org/wiki/FM-7
