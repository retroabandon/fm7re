Address Decoding
================

The [System Spec][fm7sysspec] and schematics are those referenced in the
main [README](README.md) file.


Expansion Connectors
--------------------

`EADDRBUS` and `EDATABUS` are the buffered expansion buses. Both address
and data have pull-ups on all pins. Address lines are always output-only,
and the data lines are always output except when `*MIOS` _and_ `*RWB` are
low, very much limiting the number of addresses on which one can read from
this bus.

The `*EEXTDET` input has a pull-up on it and is read from bit 0 of $FD05.

- 拡張 ("Expansion") connector `CN7`:
  - 50-pin (2×25 .1" shrouded) connector on back of unit.
  - `EDATABUS` and full `EADDRBUS` (see below). All signals buffered.
  - Other signals include: `*EEXTDET`.

- I/O card slots `CN12`, `CN13`:
  - 32-pin Fujitsu connectors; left and middle slots under cover.
  - `EDATABUS` and low 8 bits of `EADDRBUS` (see below). All signals buffered.
  - `*EIOS`: ??? select signal for card?
  - Other signals include: `*EEXTDET`.
  - All power: +5/+12/-12
  - 2.5 MHz clock

- Z80 (processor) slot `CN9`:
  - 40-pin Fujitsu connector; right-hand slot under cover.
  - Direct access to main CPU `MDATABUS` and full `MADDRBUS`.

- PSG connectors `CN8A` and `CN8B`:
  - Two 20-pin connectors on mainboard for sound card.
  - Direct access to main CPU `MDATABUS` and full `MADDRBUS`.
  - Only other signals: `EB`, `RWB`, `*RESETB`, `AUDIOOUT`.
  - 1.2 MHz clock.


M139 Main Address Space $FC00-$FFFF Decoding ROM
------------------------------------------------

M139 is a 512×4 bit ROM, an MB7053 with pull-ups on its open-collector
ouputs. (The [datasheet][mb7053] is in Japanese; the the [MB7115] is
similar if you need English information.)

The ROM is used to decode the $FC00-$FFFF area of the main CPU address
space, as shown in the extract below from the [Main ROM schematics page
(285)][sch-285-mainrom]. It's enabled when `A15`-`A10` are high and given
`A9`-`A1` on the address pins, generating one configuration of the
following active-low signals for each pair of bytes in the address range.

    *RAMIHB1    RAM inhibit
    *BTROM      Boot ROM enable
    *MIOS       Off-board I/O (do not confuse with *IOS)
    *SUBSEL     Main/Sub-CPU shared RAM select

![M139 - MB7053](dwgs/decoder-ROM-sch.jpg)


### ROM Contents

    Data: *RAMIHB1 *BTROM *MIOS *SUBSEL (●=asserted, 1=deasserted)

        ROM  ROM Data    Addr Range
      Addrs  hex  bin     Decoded     Description
    ───────────────────────────────────────────────────────────────────
    1FF       3  ●●11   FFFE - FFFF   Boot ROM   (reset vector; $FE00)
    1F8-1FE   F  1111   FFF0 - FFFD   System RAM (interrupt vectors)
    1F0-1F7   F  1111   FFE0 - FFEF   System RAM
    100-1E0   3  ●●11   FE00 - FFDF   Boot ROM   (boot code)
    0A0-0FF   5  ●1●1   FD40 - FDFF   IO: unassigned?
    09B-09F   7  ●111   FD36 - FD3F   IO: display-related registers
    098-09A   5  ●1●1   FD30 - FD35   IO
    096-097   7  ●111   FD2C - FD2F   IO
    090-095   5  ●1●1   FD20 - FD2B   IO  kanji ROM
    088-08F   5  ●1●1   FD10 - FD1F   IO: unused, FDC
    087       7  ●111   FD0E - FD0F   IO: PSG data register; RAM/ROM mode
    083-086   5  ●1●1   FD06 - FD0D   IO: RS-232; PSG cmd register
    080-082   7  ●111   FD00 - FD05   IO: kbd/printer/irqs/coprocessor control
    040-07F   6  ●11●   FC80 - FCFF   Main/Sub-CPU Shared RAM
    000-03F   F  1111   FC00 - FC7F   System RAM

    IO range $FD00-$FD7F (lower half) grid (2 bytes decoded per digit):
        080: 7775 5557 5555 5555    7=*MIOS deasserted (on-board device)
        090: 5555 5577 5557 7777    5=*MIOS   asserted (expansion slot device)

The internal/expansion slot decoding in $FD00-$FD7F appears to have some
oddities:
- $FD0D, the PSG data register, is decoded as expansion slot despite being
  an on-board device.
- $FD2C-$FD2F are decoded as internal though there is no documented
  internal device there.

Much of the above information comes from the [System Spec][fm7sysspec].
Pages 1-6 and 1-7 give the general memory map diagram and textual
explanation, and 1-8 through 1-10 lists the IO ports.

In several places the System Spec incorrectly describes $FEE0-$FEEF as
being Boot ROM; this is actually mapped to system RAM. (However, this RAM
area is not part of the CPU's vector list, which starts at $FFF0.)


Additional I/O Signals
----------------------

There are a number of additional I/O signals that are directly decoded via
gates. See, among others, the "Main I/O Address" page of the schematics.

Write selects are qualified with `*WTQE` and read selects with `*RDE`.

    Signal   Addrs      Decoded Addr. Bits   Notes
    ──────────────────────────────────────────────────────────────────────────
    *FCXX    FC00-FFFF  1111 11xx xxxx xxxx  Decode ROM chip select
    *IOS     FDxx       1111 1101 xxxx xxxx
    *FD0X    FD0x       1111 1101 0000 xxxx
    *PLTREG  FD38-FD3F  1111 1101 0011 1xxx
    *AB3                xxxx xxxx xxxx 1xxx
    *WFD05   FD05       1111 1101 0000 0101  Qual. w/E and *RWB
    *WFD37   FD37       1111 1101 0011 0111  '138, partly from *PLTREG w/o AB₃

    *RFD00                                   From dedicated '138
    *RFD01                                   (Y₆-Y₇ unused)
    *RFD02
    *RFD03
    *RFD04
    *RFD05

    *WFD00                                   From dedicated '138
    *WFD01                                   (Y₄-Y₇ unused)
    *WFD02
    *WFD03


Clock Signals
-------------

Some of the following clock signals are used to qualify decoded selects or
for other expansion devices:

    CPU  Buffered   Description
     Q      QB      Quadrature clock (1/2 cycle offset from E)
     E      EB      Clock
     BA     BAB
     BS     BSB
    R/W̅     RWB     Read (high) / write (low)

- `*RDQE`, `*WTQE`: ??? `Q`-qualified read/write?
- `Z80Φ`: `E` XOR `Q`



<!-------------------------------------------------------------------->
[fm7sysspec]: https://archive.org/stream/FM7SystemSpecifications#page/n19/mode/1up
[mb7053]: ./datasheet/6305.pdf
[mb7115]: https://archive.org/stream/bitsavers_fujitsudatmoriesDatabook_60280304#page/n980/mode/1up
[sch-285-mainrom]: https://archive.org/stream/Io19834#page/n286/mode/1up
