all: boot-rom/*.dis
.PHONY: all

boot-rom/%.dis: boot-rom/%.bin boot-rom/%.info boot-rom/common.info f9dasm/f9dasm
	f9dasm/f9dasm -cchar ';' -out $@ -info boot-rom/$*.info $<
	bin/f9post $@

#   f9dasm currently has no build system, but building it ourselves here
#   might be easier than using one provided by that repo anyway.
#   `ignore = untracked` in ../.gitmodules avoids Git seeing the submodule
#   as modified because we build `f9dasm` in the source directory.
f9dasm/f9dasm: f9dasm/f9dasm.c
	cc -o f9dasm/f9dasm f9dasm/f9dasm.c

#   Check out the submodule if that's not already been done.
f9dasm/f9dasm.c:
	git submodule update --init f9dasm

