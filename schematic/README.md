Schematics
==========

- `FM-8 Schematics IO別冊 FM-8活用研究.pdf`: From IO別冊
  [FM-8活用研究][io活用研究] (工学社, 1982), pp. 319-351.
- `FM-7 Schematics IO 1983-04.pdf`: From [FM-7 全回路図][io198304], _I/O_
  magazine, 1983-04, pp.281-300.
  - __WARNING:__ These schematics have some errors; in particular there are
    multiple places where the connecting dot is left off of connections
    between crossed lines. One obvious example is `*RDQE` on the Main CPU
    page where three gate inputs for `*WTQE`, `*RDQE` and `*RDE` are
    connected together with no input; clearly `RWB` should be driving this
    but the connection is missing.
- `fm-7/`: Hand-drawn versions of the _I/O_ magazine schematics above.
  These appear to be slightly more accurate and also have page references
  for signals coming from or going to another page. Retrieved from the
  [archive of FM-7 world][ysm72].
- `fm77av/` From  BNN Co. Ltd's "FM-Techknow" written by Sueyuki Sakai.
  Retrieved from the [archive of FM-7 World][ysm7b]

Japanese vocabulary notes:
- 別冊[P] 【べっさつ】 (n) separate volume; extra issue; supplement;
  additional volume; supplementary volume
- 活用 【かつよう】 (n,vs,vt) (1) practical use; application;
- 研究 【けんきゅう】 (n,vs,vt) study; research; investigation



<!-------------------------------------------------------------------->
[io活用研究]: https://archive.org/details/fm-8_20220609/page/319/mode/1up?view=theater
[io198304]: https://archive.org/stream/Io19834#page/n282/mode/1up
[ysm72]: https://web.archive.org/web/20151130141355/http://www23.tok2.com/home/fm7emu/ysm7/ysm72/ysm72.htm#TOP
[ysm7b]: https://web.archive.org/web/20151230233543/http://www23.tok2.com/home/fm7emu/ysm7/ysm7b/ysm7b.htm#TOP
